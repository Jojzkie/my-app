import React from 'react';
import Button from '@material-ui/core/Button';
import './App.css';
import {useRoutes} from 'hookrouter';
import Routes from './Router'

function App() {
  const routeResult = useRoutes(Routes)
  return routeResult
}

export default App;
