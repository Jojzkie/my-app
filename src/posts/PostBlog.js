export function PostBlog(postData) {
    let BaseURL = 'https://restapinodeexpressmongo.herokuapp.com/api/posts';
    //let BaseURL = 'http://localhost/PHP-Slim-Restful/api/';
  
    return new Promise((resolve, reject) =>{
    
         
        fetch(BaseURL, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            body: JSON.stringify(postData)
          })
          .then(res => res.text())
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
  
  
      });
  }