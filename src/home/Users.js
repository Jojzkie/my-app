import React, { useState, useEffect } from 'react';
import { navigate } from 'hookrouter';
import { useFetch } from "./hooks";
import Typography from '@material-ui/core/Typography';


function Users() {
  const [data, loading] = useFetch(
    "https://restapinodeexpressmongo.herokuapp.com/api/users"
  );


    return (
        <>
            <Typography component="h2" variant="h5">Users</Typography>
            {loading ? (
            "Loading..."
            ) : (
            <ul>
                {data.map((item, index) => (
                <li key={index}>
                    <p>{item.name}</p>
                    <p>{item.email}</p>
                </li>
                ))}
            </ul>
            )}
        </>
    );
}
  
export default Users;
