import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import { amber, green } from '@material-ui/core/colors';
import WarningIcon from '@material-ui/icons/Warning';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import IconButton from '@material-ui/core/IconButton';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

import {PostBlog} from './PostBlog';

export default function Form() {
    const classes = useStyles();
    const [values, setValues] = useState({title: '', description: ''}) 
    const [errorName, setErrorName] = useState('');
    const [error, setError] = useState(false);

    const handleInputChange = e => {
        const {name, value} = e.target
        setValues({...values, [name]: value})
      }
    
      const handleSubmit = event =>  {
        const blogPost = {
            title: values.title,
            description: values.description
        }
    
        console.log('Post: ' + blogPost.title );
        PostBlog(blogPost).then((result) => {
            let responseJson = result;
            alert(responseJson)
            console.log(responseJson)
        });

        event.preventDefault();
      }

  return (
    <>
        <form onSubmit={handleSubmit} className={classes.form} noValidate>
        <Typography component="h2" variant="h5">Create Post</Typography>
              <Grid item xs={12}>
                <TextField
                    required
                    fullWidth
                    name="title"
                    label="Title"
                    type="text"
                    id="title"
                    onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextareaAutosize
                    required
                    name="description"
                    label="Description"
                    type="text"
                    id="description"
                    placeholder="Description"
                    className={classes.textArea} 
                    onChange={handleInputChange}
                />
              </Grid>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onChange={handleInputChange}
            >
                Submit
            </Button>
        </form>
    </>
  );
}

function MySnackbarContentWrapper(props) {
    const classes = useStyles();
    const { className, message, onClose, variant, ...other } = props;
    const Icon = variantIcon[variant];
  
    return (
      <SnackbarContent
        className={clsx(classes[variant], className)}
        aria-describedby="client-snackbar"
        message={
          <span id="client-snackbar" className={classes.message}>
            <Icon className={clsx(classes.icon, classes.iconVariant)} />
            {message}
          </span>
        }
        action={[
          <IconButton key="close" aria-label="close" color="inherit" onClick={onClose}>
            <CloseIcon className={classes.icon} />
          </IconButton>,
        ]}
        {...other}
      />
    );
  }
  
  MySnackbarContentWrapper.propTypes = {
    className: PropTypes.string,
    message: PropTypes.string,
    onClose: PropTypes.func,
    variant: PropTypes.oneOf(['error', 'info', 'success', 'warning']).isRequired,
  };
  
  const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
  };
  
  const useStyles = makeStyles(theme => ({
      '@global': {
        body: {
          backgroundColor: theme.palette.common.white,
        },
      },
  
      errorMessage: {
        visibility: 'hidden'
      },
  
      errorMessage2: {
        visibility: 'visible'
      },

      textArea: {
          marginTop: '25px'
      },
      
      paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
      form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
      },
      submit: {
        margin: theme.spacing(3, 0, 2),
      },
      success: {
        backgroundColor: green[600],
      },
      error: {
        backgroundColor: theme.palette.error.dark,
      },
      info: {
        backgroundColor: theme.palette.primary.main,
      },
      warning: {
        backgroundColor: amber[700],
      },
      icon: {
        fontSize: 20,
      },
      iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
      },
      message: {
        display: 'flex',
        alignItems: 'center',
      },
    }));
    