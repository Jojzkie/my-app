export function PostData(type, userData) {
  let BaseURL = 'https://restapinodeexpressmongo.herokuapp.com/api/users/';
  // let BaseURL = 'http://localhost:8000/api/users/';

  return new Promise((resolve, reject) =>{
  
       
      fetch(BaseURL+type, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: JSON.stringify(userData)
        })
        .then(res => res.text())
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });


    });
}