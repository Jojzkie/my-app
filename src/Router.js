import React from "react";
import Login from  './auth/login/Login';
import Register from  './auth/register/Register';
import Home from "./home/Home";
const routes = {
  "/register": () => <Register />,
  "/login": () => <Login />,
  "/": () => <Home />,
};
export default routes;