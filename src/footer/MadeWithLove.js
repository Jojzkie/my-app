import React from 'react';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

function MadeWithLove() {
    return (
      <Typography variant="body2" color="textSecondary" align="center">
        {'Built with love by Jojie M. Jagonos - Programmer'}
      </Typography>
    );
  }

export default MadeWithLove;