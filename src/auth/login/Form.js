import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {useRoutes, useRedirect, navigate, A} from 'hookrouter';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import { amber, green } from '@material-ui/core/colors';
import WarningIcon from '@material-ui/icons/Warning';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import IconButton from '@material-ui/core/IconButton';

import {PostData} from '../PostData';

export default function Form() {
  const classes = useStyles();
  const [values, setValues] = useState({email: '', password: ''})
  const [toHome, setToHome] = useState(false);
  const [errorName, setErrorName] = useState('');
  const [error, setError] = useState(false);
  const [thanks, setThanks] = useState(false);


  useEffect(() => {
    if(sessionStorage.getItem("logout")){
      setThanks(true);
      setTimeout(function(){ 
        sessionStorage.setItem("logout",'');
        sessionStorage.clear();
        setThanks(false); 
      }, 5000);
    }
  });

  const handleInputChange = e => {
    const {name, value} = e.target
    setValues({...values, [name]: value})
  }
 
  // console.log(values.password);

  const handleSubmit = event =>  {
    const user = {
      email: values.email,
      password: values.password 
    }
    console.log('A name was submitted: ' + values.email + values.password );
    PostData('login', user).then((result) => {
      let responseJson = result;
      console.log(responseJson)
      setErrorName(responseJson)
      if(responseJson === 'Success!'){         
        sessionStorage.setItem('userData',JSON.stringify(responseJson));
        // this.setState({redirectToReferrer: true});
        setToHome(true)
      } else {
        setError(true)
      }
      
     });

    event.preventDefault();
  }

  return (
    <> 
    <Container component="main" maxWidth="xs">
        <div className={classes.paper}> 
            {toHome ? navigate('/') : null } 
            {thanks ? 
              <MySnackbarContentWrapper
                variant="success"
                className={classes.margin}
                message= "Thank You for visiting my ugly sample application!"
              /> 
            : null } 
            {error ? 
              <MySnackbarContentWrapper
                variant="error"
                className={classes.margin}
                message= {errorName}
              />  
            : null} 
            {sessionStorage.getItem('userData') ? navigate('/') : null } 
            <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
                Sign in
            </Typography>
            <form onSubmit={handleSubmit} className={classes.form} noValidate>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        value={values.email}       
                        onChange={handleInputChange}
                        autoComplete="email"
                    />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        value={values.password}       
                        onChange={handleInputChange}
                        autoComplete="current-password"
                    />
                    </Grid>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                    Sign In
                </Button>
                <Grid container justify="flex-end">
                    <Grid item>
                    <A href="/register">
                        Do not have an account? Sign up here
                    </A>
                    </Grid>
                </Grid>
            </form>
        </div>
    </Container>
    </>
  );
}


function MySnackbarContentWrapper(props) {
  const classes = useStyles();
  const { className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={clsx(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={clsx(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton key="close" aria-label="close" color="inherit" onClick={onClose}>
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

MySnackbarContentWrapper.propTypes = {
  className: PropTypes.string,
  message: PropTypes.string,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['error', 'info', 'success', 'warning']).isRequired,
};

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

const useStyles = makeStyles(theme => ({
    '@global': {
      body: {
        backgroundColor: theme.palette.common.white,
      },
    },

    errorMessage: {
      visibility: 'hidden'
    },

    errorMessage2: {
      visibility: 'visible'
    },
    
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(3),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
    success: {
      backgroundColor: green[600],
    },
    error: {
      backgroundColor: theme.palette.error.dark,
    },
    info: {
      backgroundColor: theme.palette.primary.main,
    },
    warning: {
      backgroundColor: amber[700],
    },
    icon: {
      fontSize: 20,
    },
    iconVariant: {
      opacity: 0.9,
      marginRight: theme.spacing(1),
    },
    message: {
      display: 'flex',
      alignItems: 'center',
    },
  }));
  